﻿
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Vehicles.Car;

[RequireComponent(typeof(UnityStandardAssets.Vehicles.Car.CarController))]
public class CustomCar : MonoBehaviour
{
    private CarController m_Car; // the car controller we want to use

    bool inverse = false;

	public Material[] materials;


    void Awake()
    {
        // get the car controller
        m_Car = GetComponent<UnityStandardAssets.Vehicles.Car.CarController>();
    }

    void FixedUpdate()
    {
        // pass the input to the car!
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");

        if (inverse)
        {
            h = -h;
        }

#if !MOBILE_INPUT
        float handbrake = CrossPlatformInputManager.GetAxis("Jump");
        m_Car.Move(h, v, v, handbrake);
#else
            m_Car.Move(h, v, v, 0f);
#endif
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    void OnTriggerEnter(Collider other)
    {
		Debug.Log("trigger enter " + other.tag);
        if (other.CompareTag("Bottle"))
        {
            Destroy(other.gameObject);
			//inverse = !inverse;
			//changer.Change(inverse);
        }
    }

}
