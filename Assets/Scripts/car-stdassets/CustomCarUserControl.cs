﻿
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections;
using UnityEngine.UI;


[RequireComponent(typeof(UnityStandardAssets.Vehicles.Car.CarController))]
public class CustomCarUserControl : BoCarController
{
    [Header("Display")]
    public Text SpeedText;

    [Header("BO")]
    public float delayMove = 0f;
    public float accelFactor = 1f;

    [Header("Input (Debug)")]
    public float h;
    public float v;
    public float handbrake;

    private float speedFactor;

    private UnityStandardAssets.Vehicles.Car.CarController m_Car; // the car controller we want to use

    private void Awake()
    {
        // get the car controller
        m_Car = GetComponent<UnityStandardAssets.Vehicles.Car.CarController>();
        tank = FindObjectOfType<Tank>();
        speedFactor = m_Car.MaxSpeed;
    }

    protected override void Start()
    {
        base.Start();
    }


    private void FixedUpdate()
    {
        if(logic.State != GameState.RUNNING) return;

        // get input
        h = CrossPlatformInputManager.GetAxis("Horizontal");
        if (inverseH) h *= -1;

        v = tank.CurrentFill <= 0 ? 0 : CrossPlatformInputManager.GetAxis("Vertical");
        if (inverseV) v *= -1;

        handbrake = 0f;
#if !MOBILE_INPUT
        handbrake = CrossPlatformInputManager.GetAxis("Jump");
#endif
        // update maxspeed
        if (tank.MaxSpeed > 0) m_Car.MaxSpeed = tank.MaxSpeed * speedFactor;
        // move: if delay, use coroutine
        if (delayMove > 0f) StartCoroutine(moveWithDelay(h, accelFactor * v, v, handbrake));
        else m_Car.Move(h, accelFactor * v, v, handbrake);

        // consume and update display
        consumeGas();
        updateDisplay();
        checkEnd();
    }

    public void Stop()
    {
        m_Car.Move(0,0,0,0);
    }

    private IEnumerator moveWithDelay(float h, float accel, float v, float handbrake)
    {
        yield return new WaitForSeconds(delayMove);
        m_Car.Move(h, accel, v, handbrake);
    }

    void checkEnd(){
        if(tank.MaxSpeed == 0 && m_Car.CurrentSpeed < 0.5) logic.Sober();
    }

    void updateDisplay()
    {
        if (SpeedText != null) SpeedText.text = string.Format("Speed: {0:0.00}/{1:0}", m_Car.CurrentSpeed, tank.MaxSpeed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bottle"))
        {
            Fuel bf = other.GetComponent<Fuel>();
            //if (!bf.Used) // without it, trigger called multiple times (multiple car colliders ...)
            //{
               // bf.Used = true;
                tank.Fill(bf);
                other.gameObject.SetActive(false);
                //Destroy(other.gameObject);
            //}
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "Terrain")
        {
            if(logic.State == GameState.STOPPED) m_Car.Move(0,0,0,0);
            else logic.Reload(); 
        }
    }
}

