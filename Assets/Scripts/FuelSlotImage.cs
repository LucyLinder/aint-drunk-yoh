﻿using UnityEngine;
using UnityEngine.UI;

public class FuelSlotImage : MonoBehaviour
{

    public float Speed;

    private RectTransform rectTransform;

    private Vector2 size;

    private Image image;

    public Image boImage;

    public GameObject bonusTextPrefab;

    public Color CurColor { get { return image != null ? image.color : Color.black; } set { if(image != null) image.color = value; } }

    // Use this for initialization
    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        image = GetComponent<Image>();
        if(boImage != null) boImage.enabled = false;
    }

    public void ShowBonusText(float capacity)
    {
        GameObject o = Instantiate(bonusTextPrefab, rectTransform);
        o.GetComponent<Text>().text = string.Format("+{0:0}", capacity);
        o.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
    }


    public float XEnd()
    {
        return rectTransform.localPosition.x + rectTransform.rect.width;
    }

    public float XStart()
    {
        return rectTransform.localPosition.x;
    }

    public float Width()
    {
        return rectTransform.rect.width;
    }

    public float Shrink(float byX)
    {
        float ret = size.x <= byX ? byX - size.x : 0;
        size.x -= byX;
        rectTransform.sizeDelta = size;
        return ret;
    }

    public void Set(Fuel fuel)
    {
        this.SetColor(fuel.Color);
		Speed = fuel.MaxSpeed;
    }

    public void SetColor(Color color)
    {
        if (image != null) image.color = color;
    }
    public void SetTransform(Vector2 position, float sizeX)
    {
        if(rectTransform == null) return; // ??
        transform.localPosition = position;
        size = new Vector2(sizeX, rectTransform.rect.height);
        rectTransform.sizeDelta = size;
    }

    public void OnChangeColor(Color c)
    {
        CurColor = c;
    }

    public void ShowBoImage(Sprite icon)
    {
        boImage.sprite = icon;
        boImage.enabled = true;
    }

    public void HideBoImage()
    {
        boImage.enabled = false;
    }

}
