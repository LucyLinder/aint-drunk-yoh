﻿
using UnityEngine;

public class Utils{

	public static string TimeNow ()
	{
		string now = System.DateTime.Now.ToString ("yyyy/M/d HH:mm");
		return now;
	}

	public static bool Compare (Vector3 a, Vector3 b)
	{
		return (a.x >= b.x) && (a.y >= b.y) && (a.z >= b.z);
	}

	public static string FormatTime (float seconds)
	{
		float restSecondes = seconds % 60;
		float minutes = (seconds - restSecondes) / 60;
		if ((int)restSecondes > 9)
			return "" + minutes + ":" + (int)restSecondes;
		else
			return "" + minutes + ":0" + (int)restSecondes;
	}
}
