﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class CountDown : MonoBehaviour {
	public string[] counts = new string[]{"3", "2", "1", "Go!"};
	public Text text;
	public int maxFont = 100;
	public float speed = 3;
	public float delaySeconds = 1;
	public AudioSource AudioSource;

	private int baseFont;
	private Color baseColor;
	private int curFont;

	private Image image;

	private bool active;

	public delegate void OnCountdownFinished();

	void Start () {
		baseFont = text.fontSize;
		image = GetComponent<Image>();
		baseColor = image.color;
		if(!active) gameObject.SetActive(false);
	}

	public void Run(OnCountdownFinished callback)
	{
		active = true;
		gameObject.SetActive(true);
		StartCoroutine(doCountDown(callback));
	}

	IEnumerator doCountDown(OnCountdownFinished callback)
	{
		if(delaySeconds > 0){
			text.text = "";
			yield return new WaitForSeconds(delaySeconds);
		} 

		foreach(string txt in counts)
		{
			curFont = baseFont;
			float startTime = Time.timeSinceLevelLoad;
			text.text = txt;
			if(AudioSource != null) AudioSource.Play();
			while(curFont < maxFont)
			{
				// pause countdown if pause
				if(Time.timeScale == 0){
					yield return null;
					continue;
				}

				text.fontSize = curFont;
				curFont = (int)Mathf.Lerp(baseFont, maxFont, (Time.timeSinceLevelLoad - startTime)*speed);
				yield return null;
			}
		}
		if(callback != null) callback();

		// easy fade...
		Color currentColor = baseColor;
		while(currentColor.a > 0){
			image.color = currentColor;
			currentColor.a -= 0.05f;
			yield return null;
		}

		gameObject.SetActive(false);
		active = false;

	}

	/* 
	private float scale = 1;
	public float scaleEnd = 3;
	public float scaleIncrement = 0.01f;
	RectTransform rectTransform;

	IEnumerator countDown()
	{
		foreach(string txt in counts)
		{
			scale = 1;
			text.text = txt;
			while(scale < scaleEnd)
			{
				rectTransform.localScale = Vector3.one * scale;
				scale += scaleIncrement;
				yield return null;
			}
		}
		Finish();

	}
	*/
}
