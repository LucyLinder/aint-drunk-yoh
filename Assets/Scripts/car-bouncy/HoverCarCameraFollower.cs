﻿using UnityEngine;
using System.Collections;

public class HoverCarCameraFollower : MonoBehaviour
{
    public float distance = 15f;
    public float height = 6;
    [SerializeField] Transform target;
    Vector3 camDesiredTarget;

    private LayerMask mask;

    protected virtual void Start()
    {
        mask = LayerMask.NameToLayer("camera");
    }
    protected virtual void LateUpdate()
    {
        transform.position = target.position;
        Quaternion targetRotation = Quaternion.Euler(0, target.rotation.eulerAngles.y, 0);
        transform.rotation = targetRotation;
        transform.Translate(new Vector3(0, height, -distance));

        RaycastHit hit;
        var camVector = transform.position - target.position;
        Ray ray = new Ray(target.position, camVector);
        if (Physics.Raycast(ray, out hit, distance + 0.5f, mask))
        {
            transform.position = hit.point + hit.normal;
            if(Vector3.Distance(transform.position, target.position) < 1f){
                Debug.Log("this.pos " + transform.position + "target.pos " + target.position  +"hit point " + hit.point + ", normal " + hit.normal);
            }
        }

        var rot = transform.rotation.eulerAngles;
        rot.x = Vector3.Angle(target.position - transform.position, transform.forward);
        transform.rotation = Quaternion.Euler(rot);
        transform.Translate(Vector3.forward * 0.5f);
    }
}
