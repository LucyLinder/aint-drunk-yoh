﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public float maxDistance = 15f;
    public float height = 6;
    [SerializeField] Transform target;
    Vector3 camDesiredTarget;

    private LayerMask mask;

    void Start()
    {
        mask = LayerMask.NameToLayer("camera");
    }
    void LateUpdate()
    {
        transform.position = target.position;
        Quaternion targetRotation = Quaternion.Euler(0, target.rotation.eulerAngles.y, 0);
        transform.rotation = targetRotation;
        transform.Translate(new Vector3(0, height, -maxDistance));

        RaycastHit hit;
        var camVector = transform.position - target.position;
        Ray ray = new Ray(target.position, camVector);
        if (Physics.Raycast(ray, out hit, maxDistance + 0.5f, mask))
        {
            transform.position = hit.point + hit.normal;
            if(Vector3.Distance(transform.position, target.position) < 1f){
                Debug.Log("this.pos " + transform.position + "target.pos " + target.position  +"hit point " + hit.point + ", normal " + hit.normal);
            }
        }

    }
}
