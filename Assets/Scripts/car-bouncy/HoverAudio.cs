﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverAudio : MonoBehaviour {

    public AudioSource jetSound;
    private float jetPitch;
    public float LowPitch = .1f;
    public float HighPitch = 2.0f;
    public float SpeedToRevs = .01f;
    Rigidbody carRigidbody;
    
    void Awake () 
    {
        carRigidbody = GetComponentInParent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        float forwardSpeed = transform.InverseTransformDirection(carRigidbody.velocity).z;
        float engineRevs = Mathf.Abs (forwardSpeed) * SpeedToRevs;
        jetSound.pitch = Mathf.Clamp (engineRevs, LowPitch, HighPitch);
    }

}
