﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdyCameraFollower : HoverCarCameraFollower {

	public delegate void CameraLateUpdate();
	[NonSerialized] public List<CameraLateUpdate> transformers = new List<CameraLateUpdate>();
	
	protected override void Start() {
    	base.Start();    
    }


	
	protected override void LateUpdate () {
		base.LateUpdate();
		for(int i = 0; i < transformers.Count; i++) transformers[i]();
	}
}
