﻿using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class HoverCarControl : BoCarController
{
    Rigidbody body;
    float deadZone = 0.1f; // ignore input below this limit

    [Header("Car Behavior")]
    public float groundedDrag = 3f;
    public float maxVelocity = 50;
    public float hoverForce = 1000;
    public float gravityForce = 1000f;
    public float forwardAcceleration = 8000f;
    public float reverseAcceleration = 4000f;
    public float turnStrength = 1000f;

    public float brakeDrag = 6;

    [Header("Input")]
    public bool useKeyboard = true;

    [Header("Keyboard only")]
    public float accelerationFactor = 0.01f;
    public float minTurn = 0.2f;
    public float turnIncreaseFactor = 0.01f;

    [Header("Hover And Dust")]
    public float hoverHeight = 1.5f;
    public GameObject[] hoverPoints;
    public ParticleSystem[] dustTrails = new ParticleSystem[2];

    // --- public car variables
    public bool HandbrakeOn { get {return handbrakeOn; } }

    public bool BrakeOn { get {return brakeOn; } }

    [Header("Debug")]
    // ---- private car variables
    float thrust = 0f;
    float acceleration;
    float turnAmount = 0f;
    // brakes
    bool brakeOn = false;
    bool handbrakeOn = false;
    
    float normalDrag;
    int layerMask; // to ignore collisions with self (--> car components must be in vehicle layer!)

    // ---- private ain't-druni-yoh variables

    


    protected override void Start()
    {
        base.Start();
        body = GetComponent<Rigidbody>();
        body.centerOfMass = Vector3.down;
        normalDrag = body.drag;

        layerMask = 1 << LayerMask.NameToLayer("vehicle");
        layerMask = ~layerMask;
    }

    public override void ResetPosition(Vector3 position, Quaternion rotation){
        base.ResetPosition(position, rotation);
        body.velocity = Vector3.zero;
        acceleration = 0;
        turnAmount = 0;
    }


    #region collision/trigger

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bottle"))
        {
            Fuel bf = other.GetComponent<Fuel>();
            // do nothing if the game is not running to avoid exceptions
            if(logic.State == GameState.RUNNING) tank.Fill(bf);
            other.gameObject.SetActive(false);
        }
        else if(other.gameObject.name == "Terrain")
        {
            Debug.Log("terrain !! " + logic.State);
            if (logic.State != GameState.STOPPED) logic.Reload();
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "Terrain")
        {
            if (logic.State != GameState.STOPPED) logic.Reload();
        }
    }


    #endregion

    #region input/update
    void Update()
    {
        if(logic.State == GameState.LOADING || logic.State == GameState.STOPPED) return;

        //if (tank.MaxSpeed > 0) maxVelocity = tank.MaxSpeed;
        
        // get input
        handbrakeOn = CrossPlatformInputManager.GetButton("Jump");
        float accelInput = tank.currentFill <= 0 ? 0 : CrossPlatformInputManager.GetAxis("Vertical");
        float turnInput = CrossPlatformInputManager.GetAxis("Horizontal");
        
        // inverse
        if (inverseH) turnInput *= -1f;
        if (inverseV) accelInput *= -1f;

        // update brake variable
        brakeOn = accelInput < 0f;
        
        // process input 
        if(useKeyboard) processInputKeyboard(accelInput, turnInput);
        else processInputJoystick(accelInput, turnInput);
        
        // don't allow the car to turn when it does not move
        if(acceleration == 0) turnAmount = 0;


        // show the sober panel if not moving and no gaz left
        if(tank.currentFill == 0 && acceleration == 0) logic.Sober();

        // compute final acceleration
        thrust = 0f;
        if (acceleration > deadZone)
            thrust = acceleration * forwardAcceleration * tank.MaxSpeed;
        else if (acceleration < -deadZone)
            thrust = acceleration * reverseAcceleration * tank.MaxSpeed;
    }

    void processInputKeyboard(float accelInput, float turnInput)
    {
        // fix acceleration to decelerate smoothly
        if(accelInput == 0){
            if(acceleration > 0) acceleration = Mathf.Max(acceleration - accelerationFactor, 0); 
            if(acceleration < 0) acceleration = Mathf.Min(acceleration + accelerationFactor, 0); 
        }else{
            acceleration += accelerationFactor * accelInput; 
            acceleration = Mathf.Clamp(acceleration, -1f, 1f);
        }

        // fix turning too harsh
        if(turnInput == 0){
            //if(turnAmount > 0) turnAmount = Mathf.Max(turnAmount - turnIncreaseFactor, 0); 
            //if(turnAmount < 0) turnAmount = Mathf.Min(turnAmount + turnIncreaseFactor, 0); 
            turnAmount = 0;
        }else{
            //if(accelInput < 0) turnInput *= -1; // inverse when going backward
            turnAmount += turnIncreaseFactor * turnInput; 
            turnAmount = Mathf.Clamp(turnAmount * Mathf.Abs(acceleration), -1f, 1f);
            if(Mathf.Abs(turnAmount) < minTurn) turnAmount = minTurn * (turnAmount > 0 ? 1f : -1f);
        }
    }

    void processInputJoystick(float accelInput, float turnInput)
    {
       acceleration = accelInput;
       if (Mathf.Abs(turnInput) > deadZone) turnAmount = turnInput;
       else turnAmount = 0;
    }

    #endregion


    #region move/fixedUpdate
    void FixedUpdate()
    {
        //  Do hover/bounce force
        RaycastHit hit;
        bool grounded = false;
        for (int i = 0; i < hoverPoints.Length; i++)
        {
            var hoverPoint = hoverPoints[i];
            if (Physics.Raycast(hoverPoint.transform.position, -Vector3.up, out hit, hoverHeight, layerMask))
            {
                body.AddForceAtPosition(Vector3.up * hoverForce * (1.0f - (hit.distance / hoverHeight)), hoverPoint.transform.position);
                grounded = true;
            }
            else
            {
                // Self levelling - returns the vehicle to horizontal when not grounded and simulates gravity
                if (transform.position.y > hoverPoint.transform.position.y)
                {
                    body.AddForceAtPosition(hoverPoint.transform.up * gravityForce, hoverPoint.transform.position);
                }
                else
                {
                    body.AddForceAtPosition(hoverPoint.transform.up * -gravityForce, hoverPoint.transform.position);
                }
            }
        }
        

        var emissionRate = 0;
        if (grounded)
        {
            body.drag = groundedDrag;
            emissionRate = 10;
        }
        else
        {
            body.drag = 0.1f;
            thrust /= 100f;
            turnAmount /= 100f;
        }

        for (int i = 0; i < dustTrails.Length; i++)
        {
            var emission = dustTrails[i].emission;
            emission.rate = new ParticleSystem.MinMaxCurve(emissionRate);
        }

        body.drag = handbrakeOn ? brakeDrag  : normalDrag;

        // Handle Forward and Reverse forces
        if (Mathf.Abs(thrust) > 0)
         {
            body.AddForce(transform.forward * thrust);
         }

        // Handle Turn forces
        if (turnAmount > 0)
        {
            body.AddRelativeTorque(Vector3.up * turnAmount * turnStrength);
        }
        else if (turnAmount < 0)
        {
            body.AddRelativeTorque(Vector3.up * turnAmount * turnStrength);
        }

        // Limit max velocity
        if (body.velocity.sqrMagnitude > (body.velocity.normalized * maxVelocity).sqrMagnitude)
        {
            body.velocity = body.velocity.normalized * maxVelocity;
        }
        velocity = body.velocity.sqrMagnitude;


        consumeGas();
    }
    public float velocity;
    #endregion

    #region gizmo

    // Uncomment this to see a visual indication of the raycast hit points in the editor window
    //  void OnDrawGizmos()
    //  {
    //
    //    RaycastHit hit;
    //    for (int i = 0; i < hoverPoints.Length; i++)
    //    {
    //      var hoverPoint = hoverPoints [i];
    //      if (Physics.Raycast(hoverPoint.transform.position, 
    //                          -Vector3.up, out hit,
    //                          hoverHeight, 
    //                          layerMask))
    //      {
    //        Gizmos.color = Color.blue;
    //        Gizmos.DrawLine(hoverPoint.transform.position, hit.point);
    //        Gizmos.DrawSphere(hit.point, 0.5f);
    //      } else
    //      {
    //        Gizmos.color = Color.red;
    //        Gizmos.DrawLine(hoverPoint.transform.position, 
    //                       hoverPoint.transform.position - Vector3.up * hoverHeight);
    //      }
    //    }
    //  }
    #endregion
}
