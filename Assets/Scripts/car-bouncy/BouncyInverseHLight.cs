﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BouncyInverseHLight : BouncyBrakeLight
{
    public float speed = 10f;

    protected override void Update()
    {

        if (car.inverseH)
        {
            float pong = Mathf.PingPong(Time.time * speed, 1f);
            if (type == LightType.HALO_ONLY)
            {
                halos[0].enabled = pong <= 0.5f;
                halos[1].enabled = pong > 0.5f;
            }
            else
            {
                halos[0].gameObject.SetActive(pong <= 0.5f);
                halos[1].gameObject.SetActive(pong > 0.5f);
            }
        }
        else
        {
            foreach (Behaviour h in halos) h.gameObject.SetActive(car.inverseH);
        }
    }

}
