﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BouncyBrakeLight : MonoBehaviour
{
    public enum LightType
    {
        HALO_ONLY, LIGHT_ONLY, BOTH
    }
    protected HoverCarControl car; 

    public LightType type = LightType.HALO_ONLY;

    protected List<Behaviour> halos = new List<Behaviour>();


    protected void Start()
    {
        car = GetComponentInParent<HoverCarControl>();
        for(int i = 0; i < transform.childCount; i++){
            Component c = transform.GetChild(i).GetComponent("Halo");
            if(c != null) halos.Add((Behaviour)c);
        }
        if (car == null || halos == null || halos.Count == 0)
        {
            Debug.LogError("either car controller or halos not found... Disabling myself.");
            gameObject.SetActive(false);
        }

        foreach (Behaviour h in halos) h.enabled = (type == LightType.BOTH);
    }


    protected virtual void Update()
    {
        bool brakeOn = car.BrakeOn || car.HandbrakeOn;
        // enable the halo when the car is braking, disable it otherwise.
        foreach (Behaviour h in halos)
        {
            if (type == LightType.HALO_ONLY) h.enabled = brakeOn;
            else h.gameObject.SetActive(brakeOn);
        }
    }

}
