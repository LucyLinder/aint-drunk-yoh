﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

[RequireComponent(typeof(UnityStandardAssets.ImageEffects.MotionBlur))]
public class DrunkBlur : MonoBehaviour {

	public float tankFillFor1 = 150f;
	public AnimationCurve curve;

	private Tank tank;

	private MotionBlur blur;

	void Start () {
		tank = FindObjectOfType<Tank>();
		if(tank == null){
			Debug.LogError("no tank in scene... Disabling myself.");
			gameObject.SetActive(false);
		}

		blur = GetComponent<MotionBlur>();
	}
	
	void Update () {
		float x = Mathf.Clamp(curve.Evaluate(tank.CurrentFill / tankFillFor1), 0f, 1f);
		blur.blurAmount = Mathf.Clamp(curve.Evaluate(x), 0f, 1f);
	}
}
