﻿using UnityEngine;
using UnityEngine.UI;
public class MinimapFollowPlayer : MonoBehaviour
{

    public Transform target;

	public float forwardDistance = 10;

	public float speed = 5f;

	void LateUpdate()
    {
		// player at the center (shifted by forwardDistance)
		Vector3 wantedPosition = target.position + (target.forward * forwardDistance);
        //transform.position = new Vector3(wantedPosition.x, transform.position.y, wantedPosition.z); 
		wantedPosition.y = transform.position.y;
		transform.position = Vector3.Lerp(transform.position, wantedPosition, Time.deltaTime * speed);

		// make it point to the right direction
		Vector3 angles = transform.eulerAngles;
        angles.y = target.eulerAngles.y;
		transform.eulerAngles = angles;
    }
}
