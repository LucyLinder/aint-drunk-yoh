﻿using UnityEngine;

public class Fuel : MonoBehaviour {

	public float MaxSpeed;

	public Color Color;

	public float FuelQuantity;

	public AudioClip clip;

	public bool IsWater = false;
	
}
