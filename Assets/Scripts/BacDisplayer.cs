﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class BacDisplayer : MonoBehaviour {

	private Text text;

	// Use this for initialization
	void Awake () {
		text = GetComponent<Text>();
	}
	
	public void Set(float value)
	{
		text.text = string.Format("{0:.00}", value);
		text.color = Color.Lerp(Color.green, Color.red, value);
	}
}
