﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.EventSystems;

public enum GameState
{
    LOADING, RUNNING, PAUSED, STOPPED
}

public class GameLogic : MonoBehaviour
{
    [Header("Panels")]
    public GameObject pausePanel;
    public GameObject deathPanel;
    public GameObject UI;
    public GameObject endPanel;
    public GameObject soberPanel;

    [Header("Texts")]
    public Text timeText;
    public Text deathText;
    public Text endText;
    public Image bestScoreImage;

    [Header("Death Messages")]
    public string bacTooHighMessage, noRespawnLeftMessage;

    [Header("Lives and Respawn")]
    public int maxRespawn;
    public GameObject lifePrefab;
    public GameObject lifeContainer;

    [Header("Effects")]
    public Fader fadeAnimator;
    public CountDown countdown;

    [Header("Sounds")]
    public AudioSource lifeLostSound;
    public AudioSource gameOverSound;
    public AudioSource winSound;

    // -- private variables
    // game state
    public GameState State { get { return state; } }
    private GameState state;

    // lives (displayed)
    private Stack<GameObject> lives = new Stack<GameObject>();

    // components
    private Laps laps;
    private Tank tank;

    private GameObject[] bottles;
    private EventSystem eventSystem;

    // time (for scoring)
    float playerTime, displayTime;


    private void disablePanels()
    {
        pausePanel.SetActive(false);
        deathPanel.SetActive(false);
        endPanel.SetActive(false);
        soberPanel.SetActive(false);
    }


    void Start()
    {
        eventSystem = FindObjectOfType<EventSystem>();
        laps = FindObjectOfType<Laps>();
        if (laps == null) Debug.LogError("Missing a laps in the scene !!!! NullPointerException will arise ^^.");
        tank = GetComponentInChildren<Tank>();
        if (tank == null) Debug.LogError("Missing tank.");

        bottles = GameObject.FindGameObjectsWithTag("Bottle");
        Time.timeScale = 1f;
        disablePanels();
        bestScoreImage.enabled = false;

        fadeAnimator.FadeOut(null);

        for (int i = 0; i < maxRespawn; i++) lives.Push(Instantiate(lifePrefab, lifeContainer.transform));

        runCountdown(); // countdown
    }


    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (Time.timeScale == 0) OnUnpause();
            else OnPause();
        }
    }

    void runCountdown()
    {
        if (countdown != null)
        {
            state = GameState.LOADING;
            countdown.Run(StartGame);
        }
        else
        {
            StartGame();
        }
    }

    private void StartGame()
    {
        state = GameState.RUNNING;
        playerTime = Time.timeSinceLevelLoad;
        Time.timeScale = 1f;
        InvokeRepeating("incrementTime", 0f, 1f);
    }
    private void incrementTime()
    {
        timeText.text = Utils.FormatTime(displayTime);
        displayTime += 1;
    }

    public void Reload()
    {
        if (state == GameState.RUNNING)
        {
            if (lives.Count > 0 && laps.HasMemorizedPosition())
            {
                Debug.Log("respawn");
                if(lifeLostSound != null) lifeLostSound.Play();
                respawn();
            }
            else
            {
                die(noRespawnLeftMessage);
            }
        }
    }

    public void Die()
    {
        die(bacTooHighMessage);
    }

    void die(string message)
    {
        if (state == GameState.STOPPED) return;
        Debug.Log("Dead. PlayerTime = " + (Time.timeSinceLevelLoad - playerTime));
        state = GameState.STOPPED;
        CancelInvoke("incrementTime");

        // show death panel
        UI.SetActive(false);
        deathText.text = message;
        deathPanel.SetActive(true);
        // play sound
        if(gameOverSound != null) gameOverSound.Play();
        // keyboard navigation
        giveFocusToLastButton(deathPanel);
    }

    public void Sober()
    {
        if (state == GameState.STOPPED) return;
        state = GameState.STOPPED;
        CancelInvoke("incrementTime");

        // show sober panel
        UI.SetActive(false);
        soberPanel.SetActive(true);
        // play sound
        if(gameOverSound != null) gameOverSound.Play();
        // keyboard navigation
        giveFocusToLastButton(soberPanel);
    }

    public void End()
    {
        if (state == GameState.STOPPED) return;

        Tank tank = FindObjectOfType<Tank>();

        // freeze scores
        CancelInvoke("incrementTime");
        playerTime = Time.timeSinceLevelLoad - playerTime;
        int bottlesCount = tank.BottlesCount;
        state = GameState.STOPPED;

        // save scores
        bool newBestScore = ScoresManager.AddScore(SceneManager.GetActiveScene().name, playerTime, new int[] { bottlesCount });

        // mask UI and show end panel
        UI.SetActive(false);
        endPanel.SetActive(true);
        giveFocusToLastButton(endPanel);
        

        // stacker anim
        BottlesStacker stacker = endPanel.GetComponentInChildren<BottlesStacker>();
        if (stacker != null) stacker.StackBottles(tank.bottleSprites, () =>
        {
            if(winSound != null) winSound.Play();
            endText.text = string.Format("Bottles: {0}\nTime: {1}", bottlesCount, Utils.FormatTime(playerTime)); // TODO
            bestScoreImage.enabled = newBestScore;
        });
    }


    private void giveFocusToLastButton(GameObject panel)
    {
        Button[] buttons = panel.GetComponentsInChildren<Button>();
        if (buttons.Length > 0) eventSystem.SetSelectedGameObject(buttons[buttons.Length - 1].gameObject);
    }
    private void respawn()
    {
        state = GameState.PAUSED;
        fadeAnimator.FadeIn(() =>
        {
            Destroy(lives.Pop());
            laps.ResetPosition();
            tank.Reset();
            foreach(GameObject bottle in bottles) {
                bottle.SetActive(true);
            }
            
            fadeAnimator.FadeOut(() =>
            {
                state = GameState.RUNNING;
                Debug.Log("GO!");
            });
        });
    }


    private IEnumerator reload(float seconds)
    {
        if (seconds > 0) yield return new WaitForSeconds(seconds);
        fadeAnimator.FadeIn(() => SceneManager.LoadScene(SceneManager.GetActiveScene().name));
    }

    public void OnReplay()
    {
        state = GameState.STOPPED;
        Time.timeScale = 1f;
        disablePanels();
        StartCoroutine(reload(0));
    }

    public void OnPause()
    {
        if (state == GameState.STOPPED) return;

        Time.timeScale = 0;
        state = GameState.PAUSED;

        pausePanel.SetActive(true);
        // give focus to one of the pause panel buttons for keyboard control
        Button[] pausePanelButtons = pausePanel.GetComponentsInChildren<Button>();
        eventSystem.SetSelectedGameObject(pausePanelButtons[pausePanelButtons.Length - 1].gameObject);
    }

    public void OnUnpause()
    {
        state = GameState.RUNNING;
        Time.timeScale = 1;
        pausePanel.SetActive(false);
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene("main-menu");
    }

}
