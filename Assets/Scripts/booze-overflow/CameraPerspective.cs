﻿using System;
using System.Collections;
using UnityEngine;

public class CameraPerspective : BoozeOverflower
{

    public float height;
    public float distance;
    public float changeRate;

    private AdyCameraFollower cameraFollower;
    private float originalHeight, originalDistance;

    void Start()
    {
        cameraFollower = Camera.main.GetComponent<AdyCameraFollower>();
        if (cameraFollower == null)
        {
            Debug.LogError("missing CameraSmoothFollow on main camera !");
            enabled = false;
        }
        originalHeight = cameraFollower.height;
        originalDistance = cameraFollower.distance;
    }

    private void _set(float h, float d)
    {
        cameraFollower.height = h;
        cameraFollower.distance = d;
    }

    IEnumerator set(float h, float d)
    {
        float oldH = cameraFollower.height;
        float oldD = cameraFollower.distance;

        for (float i = 0; i <= 1; i += 0.05f)
        {
            yield return null;
            cameraFollower.height = Mathf.Lerp(oldH, h, i);
            cameraFollower.distance = Mathf.Lerp(oldD, d, i);
        }
    }

    protected override void Apply()
    {
        StartCoroutine(set(height, distance));
    }

    protected override void Stop()
    {
        StartCoroutine(set(originalHeight, originalDistance));
    }

    public override string Description()
    {
        return "Camera Perspective";
    }

}
