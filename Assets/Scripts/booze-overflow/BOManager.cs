﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

[RequireComponent(typeof(Text))]
public class BOManager : MonoBehaviour
{

    [Header("Prefabs")]
    public GameObject boTextAnimationPrefab;

    [Header("Behavior")]
    public int secondsBetweenConsumption;

     [Header("Debug")]
    [NonSerialized] public List<BoozeOverflower> bos;
    [NonSerialized] public List<BoozeOverflower> activeBos;
    public float activeConsumption = 0;

    private float lastConsumptionTime;


    void Start()
    {
        // get all active bos components
        bos = GetComponentsInChildren<BoozeOverflower>().Where(o => o.gameObject.activeSelf).ToList();
        activeBos = new List<BoozeOverflower>();
    }

    public float AdditionalConsumption()
    {
        if (Time.time - lastConsumptionTime > secondsBetweenConsumption && activeBos.Count > 0)
        {
            lastConsumptionTime = Time.time;
            return activeConsumption;
        }
        return 0;
    }

    public BoozeOverflower AddEffect()
    {
        BoozeOverflower bo = getRandomOverflower();
        if(bo != null)
        {
            bo.Enable(boTextAnimationPrefab);
            activeBos.Add(bo);
            activeConsumption += bo.ConsumeLevel();
            Debug.Log("added effect " + bo.Description());
        }
        else
        {
            activeBos.Add(null); // should not happen
        }
        return bo;
    }

    public void DisableEffect()
    {
        for(int i = activeBos.Count-1; i >=0; i-- )
        {
            if(activeBos[i] != null) // if null, was already disabled
            {
                removeBoEffect(activeBos[i]);
                activeBos[i] = null; // mark it as disabled, but don't remove it !
                break;
            }
        }
    }

    public void RemoveEffect()
    {
        if (activeBos.Count > 0)
        {
            BoozeOverflower bo = activeBos.Last();
            if (bo != null)
            {
                removeBoEffect(bo);
                activeBos.Remove(bo);
            }
            else
            {
                // this bo was disabled with a water bottle, just remove it
                activeBos.RemoveAt(activeBos.Count - 1);
                Debug.Log("removed disabled effect. ");
            }
        }
    }

    private void removeBoEffect(BoozeOverflower bo)
    {
        bo.Disable();
        activeConsumption -= bo.ConsumeLevel();
        Debug.Log("removed effect " + bo.Description());
    }

    private BoozeOverflower getRandomOverflower()
    {
        // shuffle and filter actives
        List<BoozeOverflower> availableBos = bos
            .Where(b => !activeBos.Contains(b))
            .OrderBy(a => Guid.NewGuid())
            .ToList();

        if (availableBos.Count == 0)
        {
            Debug.LogError("no bo left !");
            return null;
        }
        // get one based on probas
        float proba = UnityEngine.Random.Range(0f, 1f);
        BoozeOverflower bo = availableBos
            .Where(b => b.proba <= proba)
            .FirstOrDefault();

        if (bo == null) bo = availableBos.First();
        return bo;
    }

}
