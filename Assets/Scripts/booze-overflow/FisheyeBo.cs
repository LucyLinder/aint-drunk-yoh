﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class FisheyeBo : BoozeOverflower {

	public float minX = 0.3f;
	public float maxX = 1f;
	public float minY = 0.3f;
	public float maxY = 1f;
	private Fisheye fisheye;

	void Start(){
		fisheye = Camera.main.GetComponent<Fisheye>();
		if(fisheye == null){
			Debug.LogError("Main camera missing the Fisheye component !");
			enabled = false;
		}
		Stop(); // ensure fisheye is at 0
	}
	
    protected override void Apply()
    {
		StartCoroutine(smoothApply(UnityEngine.Random.Range(minX, maxX), UnityEngine.Random.Range(minY, maxY)));
        /*if(fisheye != null){
			fisheye.strengthX = UnityEngine.Random.Range(minX, maxX);
			fisheye.strengthY = UnityEngine.Random.Range(minY, maxY);
		}*/
    }

    protected override void Stop()
    {
		StartCoroutine(smoothDisable());
        /*if(fisheye != null){
			fisheye.strengthX = 0f;
			fisheye.strengthY = 0f;
		}*/
    }


    public override string Description()
    {
        return "Fisheye";
    }

	IEnumerator smoothApply(float strengthX, float strengthY)
	{
		float startTime = Time.time;
		while(IsActive && fisheye.strengthX < strengthX || fisheye.strengthY < strengthY)
		{
			fisheye.strengthX = Mathf.Lerp(0, strengthX, Time.time - startTime);
			fisheye.strengthY = Mathf.Lerp(0, strengthY, Time.time - startTime);
			yield return null;
		}
	}

	IEnumerator smoothDisable()
	{
		float startTime = Time.time;
		float strengthX = fisheye.strengthX;
		float strengthY= fisheye.strengthY;

		while(!IsActive && fisheye.strengthX > 0 || fisheye.strengthY > 0)
		{
			fisheye.strengthX = strengthX - Mathf.Lerp(0, strengthX, Time.time - startTime);
			fisheye.strengthY = strengthY - Mathf.Lerp(0, strengthY, Time.time - startTime);
			yield return null;
		}
	}
}
