﻿using UnityEngine;

public class InvertHBo : BoozeOverflower {

	BoCarController ccc;

	void Start()
	{
		ccc = GameObject.FindWithTag("Player").GetComponent<BoCarController>();
	}

	protected override void Apply()
	{
		ccc.inverseH = true;
	}

	protected override void Stop()
	{
		ccc.inverseH = false;
	}

	    public override string Description()
    {
        return "Horizontal Inversion";
    }
}
