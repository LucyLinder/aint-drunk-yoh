﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowBo : BoozeOverflower
{

    public List<Transform> objectsToGrow;
    public float speed = 0.01f;

    public float scaleFactor;
    private Vector3[] originalScales;

    private float currentFactor = 1;

    void Start()
    {
        originalScales = new Vector3[objectsToGrow.Count];
        for (int i = 0; i < originalScales.Length; i++) originalScales[i] = objectsToGrow[i].localScale;
    }

    private float t; // "time" for lerp

    protected override void DoUpdate()
    {
        if (IsActive)
        {
            currentFactor = Mathf.PingPong(t, scaleFactor - 1) + 1;
            for (int i = 0; i < originalScales.Length; i++) objectsToGrow[i].localScale = originalScales[i] * currentFactor;
            t += speed;

        }
        else if (currentFactor > 1f)
        {
            currentFactor -= speed;
            for (int i = 0; i < originalScales.Length; i++) objectsToGrow[i].localScale = originalScales[i] * currentFactor;
        }
    }
    protected override void Apply()
    {
        currentFactor = 1;
        t = 0.5f;
    }

    protected override void Stop()
    {
        //objectToGrow.localScale = originalScale;
    }

    public override string Description()
    {
        return "Growing";
    }

}
