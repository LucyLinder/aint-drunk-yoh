﻿using System;
using System.Collections;
using UnityEngine;


public class TransparentBO : BoozeOverflower {
    public GameObject skyCarBody;

    protected override void Apply()
    {
        skyCarBody.SetActive(false);        
    }

    protected override void Stop()
    {
        skyCarBody.SetActive(true);
    }

    public override string Description()
    {
        return "Transparency";
    }
}
