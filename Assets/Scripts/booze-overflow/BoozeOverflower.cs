﻿using System;
using UnityEngine;
using UnityEngine.UI;

public abstract class BoozeOverflower : MonoBehaviour
{
    public Sprite icon;
    public float proba = 1;

    public float consumption = 1;

    public bool IsActive { get { return isActive; } }
    private bool isActive;

    private float startActiveTime;
    private float effectDelay = 0.5f;

    [NonSerialized] public GameObject iconObject;
    [NonSerialized] public GameObject textAnim;
    [NonSerialized] public bool isAnimPlaying;
    public void Enable(GameObject animPrefab)
    {
        isAnimPlaying = true;
        textAnim = Instantiate(animPrefab, GetComponentInParent<Canvas>().transform);
        textAnim.GetComponent<Text>().text = Description();
        startActiveTime = Time.timeSinceLevelLoad;
    }

    void Update(){
        if(isAnimPlaying && (Time.timeSinceLevelLoad - startActiveTime > effectDelay)){ //textAnim == null){
            isAnimPlaying = false;
            isActive = true;
            Apply();
        }
        DoUpdate();
    }

    public void Disable()
    {
        if(textAnim != null) Destroy(textAnim);
        isAnimPlaying = false;
        isActive = false;
        Stop();
    }

    public float Proba()
    {
        return proba;
    }
    
    public abstract string Description();

    public float ConsumeLevel()
    {
        return consumption;
    }

    protected abstract void Apply();

    protected abstract void Stop();

    protected virtual void DoUpdate(){

    }

}
