﻿using UnityEngine;

public class InvertVBo : BoozeOverflower {

	BoCarController ccc;

	void Start()
	{
		ccc = GameObject.FindWithTag("Player").GetComponent<BoCarController>();
	}

	protected override void Apply()
	{
		ccc.inverseV = true;
	}
	
	protected override void Stop()
	{
		ccc.inverseV = false;
	}

	public override string Description()
    {
        return "Vertical Inversion";
    }
}
