﻿using System.Collections;
using UnityEngine;

public class CameraRotate : BoozeOverflower
{

    public Vector3 skewing;
    public float smoothDecrease = 0.01f;
    private Quaternion currentSkew;
    private Quaternion from, to;
    public float speed = 0.1F;
    private AdyCameraFollower cameraFollower;
    private Transform camTransform;

    private float t;

    void Start()
    {
        cameraFollower = Camera.main.GetComponent<AdyCameraFollower>();
        if (cameraFollower == null)
        {
            Debug.LogError("missing AdyCameraFollower on main camera !");
            enabled = false;
        }

        camTransform = Camera.main.transform;
        from = new Quaternion(skewing.x, skewing.y, skewing.z, 1);
        to = Quaternion.Inverse(from);
    }


    protected override void Apply()
    {
        t = 0;
        cameraFollower.transformers.Add(skew);
    }

    protected override void Stop()
    {
        
    }

    private void skew()
    {
        if (IsActive)
        {
            currentSkew = Quaternion.Lerp(from, to, Mathf.PingPong(Time.time, 1));
            camTransform.rotation *= currentSkew;
        }
        else if(t < 1)
        {
            t += smoothDecrease;
            camTransform.rotation *= Quaternion.Lerp(currentSkew, Quaternion.identity, t);
        }
        else
        {
            cameraFollower.transformers.Remove(skew);
        }
    }

    public override string Description()
    {
        return "Camera Rotate";
    }
}
