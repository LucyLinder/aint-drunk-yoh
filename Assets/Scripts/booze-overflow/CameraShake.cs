﻿using UnityEngine;
using System;

public class CameraShake : BoozeOverflower
{

    public float shakeAmount = 0.7f;
    private Transform camTransform;
    Vector3 originalPos;
    private AdyCameraFollower cameraFollower;

    void Start()
    {
        cameraFollower = Camera.main.GetComponent<AdyCameraFollower>();
        if (cameraFollower == null)
        {
            Debug.LogError("missing AdyCameraFollower on main camera !");
            enabled = false;
        }
        camTransform = Camera.main.transform;
    }

    private void shake()
    {
        camTransform.localPosition += UnityEngine.Random.insideUnitSphere * shakeAmount;
    }


    protected override void Apply()
    {
        cameraFollower.transformers.Add(shake);
    }

    protected override void Stop()
    {
        cameraFollower.transformers.Remove(shake);
    }


    public override string Description()
    {
        return "Shaking";
    }
}