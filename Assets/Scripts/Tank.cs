﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Tank : MonoBehaviour
{

    public float MaxSpeed { get { return maxSpeed; } }

    public int BottlesCount { get { return bottlesCount; } }

    public float CurrentFill { get { return currentFill; } }

    public bool IsFull { get { return currentFill >= tankCapacity; } }

    public bool IsEmpty { get { return currentFill <= 0; } }

    public float SizeToCapacity(float size) { return size / tankDisplayFactor; }

    [Header("Tank Display")]
    public GameObject tankContainer;
    public GameObject slotPrefab;
    public Text waterCountText;

    public float insets = 5;

    [Header("Tank Behavior")]
    public float tankCapacity = 100;

    public float deathLimit = 300;

    public float currentFill;

    public float overflowOffset;

    [Header("Audio")]
    public AudioSource audioDrinkSource;

    [Header("Debug")]
    // ---- private 
    private float initFill;


    private RectTransform rectTransform;
    private BacDisplayer bacDisplayer;
    private BOManager boManager;

    private float maxSpeed;
    private float tankDisplayFactor;
    private Stack<FuelSlotImage> fuel = new Stack<FuelSlotImage>();
    private int bottlesCount;

    private int waterCount = 0;

    [NonSerialized] public List<Sprite> bottleSprites = new List<Sprite>();

    void Start()
    {
        boManager = FindObjectOfType<BOManager>();
        bacDisplayer = GetComponentInChildren<BacDisplayer>();
        rectTransform = tankContainer.GetComponent<RectTransform>();
        tankDisplayFactor = (rectTransform.rect.width - 2 * insets) / tankCapacity;
        initFill = currentFill;
        Reset();
        updateWaterCountDisplay();
    }


    public void Reset()
    {
        while (fuel.Count > 0)
        {
            Destroy(fuel.Pop().gameObject);
            boManager.RemoveEffect();
        }
        currentFill = 0; // since fill already updates currentFill...
        if (initFill > 0) Fill(initFill, Color.yellow, 1f);
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            UseWater();
        }
    }

    public void Fill(Fuel fuelSlot)
    {
        if (fuelSlot.IsWater)
        {
            PickWater();
        }
        else
        {
            updateBottlesCount(fuelSlot);
            this.Fill(fuelSlot.FuelQuantity, fuelSlot.Color, fuelSlot.MaxSpeed);
            fuel.Peek().ShowBonusText(fuelSlot.FuelQuantity);
            playDrinkSound(fuelSlot);
        }
        // for win sound end testing:
        //for(int i = 0; i < 10; i++) bottleSprites.Add(fuelSlot.gameObject.GetComponent<SpriteRenderer>().sprite);
        //FindObjectOfType<GameLogic>().End();
    }

    public void Fill(float cap, Color color, float speed)
    {

        GameObject newSlot = Instantiate(slotPrefab, rectTransform);

        FuelSlotImage fuelImage = newSlot.GetComponent<FuelSlotImage>();
        float x = fuel.Count == 0 ? insets : fuel.Peek().XEnd() + 2;
        fuelImage.SetTransform(new Vector2(x, -insets), cap * tankDisplayFactor);
        fuelImage.SetColor(color);
        fuelImage.Speed = speed;
        fuel.Push(fuelImage);

        currentFill += cap;
        if (currentFill >= deathLimit) FindObjectOfType<GameLogic>().Die();
        updateBac();

        slotsChanged();

        // add overflow if tank full (avoid "blinking effect" with an offset)
        if (IsFull && currentFill >= tankCapacity + overflowOffset)
        {
            BoozeOverflower bo = boManager.AddEffect();
            if (bo != null) fuelImage.ShowBoImage(bo.icon);
        }

    }

    public void UseWater()
    {
        Debug.Log("using water");
        if (IsFull && waterCount > 0)
        {
            boManager.DisableEffect();
            FuelSlotImage[] slt = fuel.ToArray(); 
            // find the first enabled BO icon and disable it
            // TODO: not very clean, find a cleaner way
            for(int i = 0; i < slt.Length; i++ ){
                if(slt[i].boImage.enabled)
                {
                    slt[i].boImage.enabled = false;
                    break;
                }
            }
            waterCount--;
            updateWaterCountDisplay();
        }
    }

    public void PickWater()
    {
        waterCount++;
        updateWaterCountDisplay();
    }

    void updateWaterCountDisplay()
    {
        if (waterCountText != null) waterCountText.text = "" + waterCount;
    }

    void updateBottlesCount(Fuel fuelSlot)
    {
        bottlesCount++;
        bottleSprites.Add(fuelSlot.gameObject.GetComponent<SpriteRenderer>().sprite);
    }

    public bool Consume(float consumption)
    {
        consumption += boManager.AdditionalConsumption();

        int len = fuel.Count;
        float shrink = consumption * tankDisplayFactor;
        while (fuel.Count > 0)
        {
            FuelSlotImage f = fuel.Peek();
            shrink = f.Shrink(shrink);
            if (shrink == 0)
            {
                break;
            }
            // discard empty slot 
            Destroy(fuel.Pop().gameObject);
            boManager.RemoveEffect();
        }

        bool wasFull = IsFull;
        currentFill = Mathf.Max(currentFill - consumption, 0f);
        if (wasFull && !IsFull)
        {
            boManager.RemoveEffect();
            fuel.Peek().boImage.enabled = false;
        }

        if (len != fuel.Count) slotsChanged();
        updateBac(); //uncomment this for realtime BAC update

        return currentFill > 0;
    }

    void slotsChanged()
    {
        float speed = 0;
        Color c = Color.black;
        foreach (FuelSlotImage fuelSlot in fuel)
        {
            c += fuelSlot.CurColor;
            speed += fuelSlot.Speed;
        }
        //updateBac();
        notifyChangeColor(c / fuel.Count);
        maxSpeed = fuel.Count == 0 ? 0 : speed / fuel.Count;
    }

    void updateBac()
    {
        if (bacDisplayer != null) bacDisplayer.Set(currentFill / deathLimit);
    }

    private void notifyChangeColor(Color newColor)
    {
        BroadcastMessage("OnChangeColor", newColor, SendMessageOptions.DontRequireReceiver);
    }

    private void playDrinkSound(Fuel fuel)
    {
        if (audioDrinkSource != null && fuel.clip != null)
        {
            audioDrinkSource.Stop();
            audioDrinkSource.PlayOneShot(fuel.clip);
        }
    }

}
