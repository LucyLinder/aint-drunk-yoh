﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.UI;

public class ProgressGui : MonoBehaviour
{
    //public float barDisplay; //current progress

    [Header("Tank")]
    public float tankCapacity = 100f;
    public float tankFill = 0;

    public bool tankFull { get { return tankFill >= tankCapacity; } }

    public bool tankEmpty { get { return tankFill <= 0; } }

    [Header("Display")]
    public Rect tankSize;
    public float tankBorders;
    public float maxTankOverflow;

    public Texture2D emptyTex;
    public static Color defaultColor = Color.yellow;


    private Rect _tankRect, _drawingArea;

    public enum TankState
    {
        EMPTY, OK, FULL
    }

    private TankState state;


    private Stack<FuelSlot> fuel = new Stack<FuelSlot>();

    public float TankSizeToDisplaySize(float cap){
        return tankSize.width / tankCapacity * cap;
    }

    class FuelSlot
    {
        public Color color;

        public float maxSpeed;

        public float capacity;

        public FuelSlot(float cap, Color c, float speed)
        {
            this.capacity = cap;
            this.color = c;
            this.maxSpeed = speed;

        }

        public FuelSlot(Fuel bf) : this(bf.FuelQuantity, bf.Color, bf.MaxSpeed) { }
    }

    void Start()
    {
        Fill(50, defaultColor, 1);
        _tankRect = new Rect(tankSize.position.x - tankBorders, tankSize.position.y - tankBorders, tankSize.width + 2 * tankBorders, tankSize.height + 2 * tankBorders);
        _drawingArea = new Rect(tankSize);
        _drawingArea.width = maxTankOverflow + 2 * tankBorders;
    }


    void OnGUI()
    {
        GUI.depth = 100;
        GUI.Box(_tankRect, emptyTex);
        GUI.BeginGroup(_drawingArea);
        //draw the filled-in part:
        float orig = 0;
        foreach (FuelSlot f in fuel.Reverse())
        {
            float displaySize = TankSizeToDisplaySize(f.capacity);
            GUItils.DrawRect(new Rect(orig, 0, displaySize, tankSize.width), f.color);
            orig += displaySize;
        }
        GUI.EndGroup();
    }

    public float MaxSpeed()
    {
        if (fuel.Count > 0) return fuel.Peek().maxSpeed;
        return 0;
    }

    public bool Consume(float consumption)
    {
        tankFill = Mathf.Max(tankFill - consumption, 0f);

        while (fuel.Count > 0 && consumption > 0)
        {
            FuelSlot f = fuel.Peek();
            if (f.capacity < consumption)
            {
                // discard empty slot 
                consumption -= f.capacity;
                fuel.Pop();
            }
            else
            {
                // consume current slot
                f.capacity -= consumption;
                consumption = 0;
            }
        }

        updateState();
        return tankFill > 0;
    }

    public void notifyTankStateChange()
    {
        BroadcastMessage("OnTankStateChange", state);
    }

    public void Fill(float cap, Color color, float speed)
    {
        tankFill += cap;
        fuel.Push(new FuelSlot(cap, color, speed));
        updateState();
    }
    public void Fill(Fuel bottle)
    {
        this.Fill(bottle.FuelQuantity, bottle.Color, bottle.MaxSpeed);
    }

    private void updateState()
    {
        TankState oldState = state;
        if (tankFull)
        {
            state = TankState.FULL;
        }
        else if (tankEmpty)
        {
            state = TankState.EMPTY;
        }
        else
        {
            state = TankState.OK;
        }
        if (oldState != state) notifyTankStateChange();
    }

    void OnDrawGizmos()
    {     
         Gizmos.DrawGUITexture(tankSize, Texture2D.whiteTexture);
    }

}