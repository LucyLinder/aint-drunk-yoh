﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialChanger : MonoBehaviour {

	public Material[] materials;
	private Renderer rend;
	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();
		Change(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Change(bool inversed){
		if(materials.Length >= 2){
			rend.material = inversed ? materials[1] : materials[0]; //.color = inversed ? Color.red : Color.yellow;
		}
	}
}
