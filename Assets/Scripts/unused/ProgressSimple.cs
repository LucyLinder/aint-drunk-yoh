﻿using UnityEngine;

public class ProgressSimple : MonoBehaviour
{
    public float barDisplay; //current progress
    public Vector2 pos = new Vector2(20, 40);
    public Vector2 size = new Vector2(60, 20);
    public Texture2D emptyTex;
    public static Color defaultColor;

    private float lastUpdate;

    void Start()
    {
        barDisplay = 1;
        lastUpdate = Time.time;
    }


    void OnGUI()
    {
        //draw the background:

        GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y));
        GUI.Box(new Rect(0, 0, size.x, size.y), emptyTex);
        //draw the filled-in part:
        GUI.BeginGroup(new Rect(0, 0, size.x * barDisplay, size.y));
        GUItils.DrawRect(new Rect(0, 0, size.x * barDisplay, size.y), Color.yellow);
        GUI.EndGroup();
        GUI.EndGroup();
    }

    void Update()
    {
        //for this example, the bar display is linked to the current time,
        //however you would set this value based on your desired display
        //eg, the loading progress, the player's health, or whatever.
        if (Time.time - lastUpdate > 1)
        {
            if (barDisplay > 0) barDisplay -= 0.005f;
            lastUpdate = Time.time;
        }

        //        barDisplay = MyControlScript.staticHealth;
    }
}