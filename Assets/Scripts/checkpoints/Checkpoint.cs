﻿using System.Collections;
using UnityEngine;

public class Checkpoint : MonoBehaviour {

	public int id;

	private Laps laps;

	//private int vehicleLayer;

	void Start()
	{
		laps = transform.parent.GetComponentInChildren<Laps>();
		//vehicleLayer = LayerMask.NameToLayer("vehicle");
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player")) laps.CheckpointPassed(this, other.gameObject); //ok only if collider properly tagged.
	}
}
