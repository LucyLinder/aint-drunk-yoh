﻿using UnityEngine;
using UnityEngine.UI;

public class Laps : MonoBehaviour {

	public Text lapsDisplayText;
	public int numberLaps = 1;

	private int numberOfCheckpoints;
	private int currentLap = 0;
	private int nextCheckpoint = 0;

	private GameObject player;
	private Vector3 lastCarPosition;
	private Quaternion lastCarRotation;

	private SpriteRenderer spriteRenderer;

	void Start () {
		spriteRenderer = GetComponentInChildren<SpriteRenderer>();
		// automatically discover checkpoints
		Checkpoint[] checkpoints = transform.parent.GetComponentsInChildren<Checkpoint>();
		for(int i = 0; i < checkpoints.Length; i++) checkpoints[i].id = i;
		numberOfCheckpoints = checkpoints.Length;
		// update text
		updateLapDisplay();
		// hide sprite line
		if(spriteRenderer != null) spriteRenderer.enabled = false;
		else Debug.LogError("no sprite in endline");
		// get player position at start?
		player = GameObject.FindWithTag("Player");
		lastCarPosition = player.transform.position;
		lastCarRotation = player.transform.rotation;
	}
	
	public void CheckpointPassed(Checkpoint c, GameObject player){

		if(nextCheckpoint == -1) nextCheckpoint = c.id; //respawn flag

		if(c.id == nextCheckpoint){
			if(c.id == numberOfCheckpoints - 1 && spriteRenderer != null) spriteRenderer.enabled = true;
			Debug.Log("passed checkpoint " + c.id);
			memorizePosition(c.gameObject, player);
			nextCheckpoint++;
		}else{
			Debug.LogError("missed a checkpoint. Expected: " + nextCheckpoint + ", got " + c.id);
		}
	}

	void memorizePosition(GameObject checkpoint, GameObject player)
	{
		lastCarPosition = new Vector3(checkpoint.transform.position.x, player.transform.position.y, checkpoint.transform.position.z);
		lastCarRotation = checkpoint.transform.rotation; //player.transform.rotation;
		Debug.Log("position memorized " + checkpoint.transform.position);
	}


	public bool HasMemorizedPosition(){
		return player != null;
	}

	public void ResetPosition(){
		player.GetComponent<HoverCarControl>().ResetPosition(lastCarPosition, lastCarRotation);
		//nextCheckpoint = Mathf.Min(1, nextCheckpoint - 1); 
		nextCheckpoint = -1; // respawn flag
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player")){
			if(nextCheckpoint == numberOfCheckpoints){
				if(spriteRenderer != null) spriteRenderer.enabled = false;
				currentLap++;
				updateLapDisplay();

				if(currentLap >= numberLaps){
					// it was the final lap => end
					FindObjectOfType<GameLogic>().End();
				}else{
					// still some lap to go 
					nextCheckpoint = 0;
					memorizePosition(this.gameObject, other.gameObject);
				}

			}else{
				Debug.LogError("player passed the endline without passing all the checkpoints: " + nextCheckpoint);
			}
		}
	}

	void updateLapDisplay(){
		if(lapsDisplayText != null) lapsDisplayText.text = (currentLap+1) + "/" + (numberLaps);
	}
}
