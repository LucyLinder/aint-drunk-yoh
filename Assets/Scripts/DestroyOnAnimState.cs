﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DestroyOnAnimState : MonoBehaviour {

	public string animName = "End";
	private Animator anim;

	void Start () {
		anim = GetComponent<Animator>();
	}
	
	void Update () {
		if(anim.GetCurrentAnimatorStateInfo(0).IsName(animName)){
			Destroy(gameObject);
		}
	}
}
