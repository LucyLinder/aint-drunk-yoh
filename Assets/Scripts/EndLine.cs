﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndLine : MonoBehaviour
{

    public int numberOfRounds = 0;
    private int rounds = 0;

    private GameLogic logic;

    private bool end;

    // Use this for initialization
    void Start()
    {
        logic = FindObjectOfType<GameLogic>();
		rounds = 0;
    }


    void OnTriggerEnter(Collider other)
    {
        if (end || !enabled) return;
        if (other.CompareTag("Player"))
        {
            rounds++;
			Debug.Log("round " + rounds);
            if (rounds >= numberOfRounds)
            {
                end = true;
                Debug.Log("END !!");
				logic.End();
            }
        }
    }
}
