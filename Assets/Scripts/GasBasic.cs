﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasBasic : MonoBehaviour
{

    public float gas; //in liters because unity uses metric system
    float KPL; //kilometers per liter, basically consumption rate
    Vector3 LastPosition; //this will just store our last position
    Vector3 CurrentPosition; //transform.position
    float change; //how many kilometers we went between updates;

    public Tank progress;

    void Start()
    {
        LastPosition = transform.position;
        CurrentPosition = transform.position;
        change = 0f;
        gas = 10f; //the f just means 10 is a float number not an int
        KPL = 10f;
    }

    //were going to do this in fixed update so that 
    //the change between frames isn't really small and
    //subject to having really small changes being rounded 
    //down to zero.
    void FixedUpdate()
    {
        CurrentPosition = transform.position;

        change = (CurrentPosition - LastPosition).sqrMagnitude; //this is in meters. we multiply by 1000 to get kilometers
        //change = change * 1000;
        //change now represents how much gas we've used up between frames

        gas = gas - Mathf.Abs(change) / KPL;

        //now we'll save our current position as our last 
        //so we can make use of it in the next update to 
        //calculate the change in distance and know how much gas 
        //was used

        LastPosition = CurrentPosition;

        if (gas <= 0)
        {
            //disable your movement script for you car, basically 
            //without gas you can't move
        }

    }
}


 /*next we'll make a second script this will be attached to another object this will be the gas station. The gas station will have attached to it a collider MARKED AS A TRIGGER we will use the function OnTriggerEnter(Collider collider) to make it so when an object enters the bounds of the collider we check the object tag to see if it's a car and if so we increase its gas variable. Gas was earlier made public so we could modify it.
 
 
 
 float HowMuchGasYouGetFromTheStation = 10;
 
 
 void OnTriggerEnter(Collider collider){
 if(collider.gameobject.tag == "car")
 {
 collider.gameobject.getcomponent<nameOfGasScript>().gas += HowMuchGasYouGetFromTheStation;
 
 
 }
 
 this script is loaded with some issues. you can keep getting gas there is no maximum and stuff but its a great solid starting point to modify off of.
}
*/
