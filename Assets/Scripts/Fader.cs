﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Fader : MonoBehaviour
{

    public delegate void OnFadeEnd();
    public float FadeDuration = 2f;

    private Image image;
    // Use this for initialization
    void Awake()
    {
        image = GetComponent<Image>();
    }

    public void FadeOut(OnFadeEnd callback)
    {
        StartCoroutine(fade(FadeDuration, 1, 0, callback));
    }

    public void FadeIn(OnFadeEnd callback)
    {
        StartCoroutine(fade(FadeDuration, 0, 1, callback));
    }


    private IEnumerator fade(float duration, float startAlpha, float endAlpha, OnFadeEnd callback)
    {
        // remember the start
        float start = Time.time;
        float elapsed;

        Color startColor, endColor;
        startColor = endColor = image.color;
        startColor.a = startAlpha;
        endColor.a = endAlpha;

        do
        {  // calculate how far through we are
            elapsed = Time.time - start;
            float normalisedTime = Mathf.Clamp(elapsed / duration, 0, 1);
            image.color = Color.Lerp(startColor, endColor, normalisedTime);
            // wait for the next frame
            yield return null;
        } while (elapsed <= duration);

        if(callback != null) callback();
    }

   
}
