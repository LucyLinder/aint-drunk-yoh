﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottlesStacker : MonoBehaviour
{

    public GameObject stackedBottlePrefab;

	public float delayBetweenBottles = .8f;

	private AudioSource audioSource;

	public delegate void OnStackingEnd();

	void Start()
	{
		audioSource = GetComponent<AudioSource>();
	}

    public void StackBottles(List<Sprite> bottles, OnStackingEnd callback)
    {
        StartCoroutine(doStackBottles(bottles, callback));
    }

	IEnumerator doStackBottles(List<Sprite> bottles, OnStackingEnd callback)
    {
        foreach (Sprite sprite in bottles)
        {
			yield return new WaitForSeconds(delayBetweenBottles);
			audioSource.Play();
            GameObject o = Instantiate(stackedBottlePrefab, transform);
			Image img = o.GetComponent<Image>();
			img.sprite = sprite;
			img.preserveAspect = true;
        }

		if(callback != null)
		{
			yield return new WaitForSeconds(delayBetweenBottles);
			callback();
		}
    }
}
