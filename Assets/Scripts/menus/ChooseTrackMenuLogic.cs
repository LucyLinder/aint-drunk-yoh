﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChooseTrackMenuLogic : MonoBehaviour
{

    public GameObject tracksPanel, scoresPanel;
    public ScoresTable scoreTable;

    private CanvasGroupFader scoresPanelFader, tracksPanelFader;

    private EventSystem eventSystem;
    private GameObject lastSelected = null;

    private bool isScoresPanelShowing = false;
    void Start()
    {        
        scoresPanelFader = scoresPanel.GetComponent<CanvasGroupFader>();
        tracksPanelFader = tracksPanel.GetComponent<CanvasGroupFader>();

        // show/ide proper panel
        scoresPanel.GetComponent<CanvasGroup>().alpha = 0f;
        tracksPanel.GetComponent<CanvasGroup>().alpha = 1f;

        eventSystem = FindObjectOfType<EventSystem>();
    }

    void Update(){
        // use escape to return to main menu
        if (Input.GetButtonDown("Cancel"))
        {
            if(isScoresPanelShowing) ShowTrackPanel();
            else BackToMainMenu();
        }
    }

    public void ShowScoresPanel(string level)
    {
        tracksPanelFader.FadeOut(() => tracksPanel.SetActive(false));
        scoresPanelFader.FadeIn(null);
        scoreTable.Show(level);
        Button scorePanelButton = scoresPanel.GetComponentInChildren<Button>();
        lastSelected = eventSystem.currentSelectedGameObject;
        eventSystem.SetSelectedGameObject(scorePanelButton.gameObject);
        isScoresPanelShowing = true;
    }

    public void ShowTrackPanel()
    {
        tracksPanel.SetActive(true);
        tracksPanelFader.FadeIn(null);
        scoresPanelFader.FadeOut(null);   
        isScoresPanelShowing = false;
        if(lastSelected != null){
            eventSystem.SetSelectedGameObject(lastSelected);
            lastSelected = null;
        }   
    }

    public void BackToMainMenu(){
        SceneManager.LoadScene("main-menu");
    }
}
