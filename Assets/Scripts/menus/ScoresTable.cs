﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoresTable : MonoBehaviour
{

	public GameObject prefab;
	public GameObject noScore;
	private List<GameObject> cells;

	public void Show (string level)
	{
		Hide(); // remove all cells 

		ScoreData scoreData = ScoresManager.getScoreData(level);
		float[] scores = scoreData.Scores;
		string[] meta = scoreData.Stats;

		if (scores.Length == 0) {
			gameObject.SetActive (false);
			noScore.SetActive (true);
			return;
		}

		gameObject.SetActive (true);
		noScore.SetActive (false);

		cells = new List<GameObject>();
		for (int i = 0; i < scores.Length; i++) {
			// score
			GameObject o = Instantiate (prefab, gameObject.transform);
			o.GetComponent<Text> ().text = Utils.FormatTime (scores [i]);
			string[] fields = meta [i].Split ('#');
			cells.Add(o);

			// purge
			o = Instantiate (prefab, gameObject.transform);
			o.GetComponent<Text> ().text = string.Format ("{0}", fields [1]);	
			cells.Add(o);

			// date
			o = Instantiate (prefab, gameObject.transform);
			o.GetComponent<Text> ().text = fields [0];	
			cells.Add(o);

		}
	}

	void Hide(){
		/*List<GameObject> cells = new List<GameObject>();
		foreach(Transform t in transform) cells.Add(t.gameObject);
		Debug.Log("destroying " + cells.Count);
		cells.ForEach(o => Destroy(o));*/
		if(cells != null) cells.ForEach(o => Destroy(o));
		cells = null;
	}

}
