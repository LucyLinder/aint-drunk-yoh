﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChooseTrackMenuEventTrigger : EventTrigger
{

    private MySliderMenu menu;
    void Start()
    {
        menu = FindObjectOfType<MySliderMenu>();
    }

    public override void OnMove(AxisEventData data)
    {
        switch (data.moveDir)
        {
            case MoveDirection.Right: menu.NextButton(); break;
            case MoveDirection.Left: menu.PreviousButton(); break;
            default: base.OnMove(data); break;
        }
    }
}
