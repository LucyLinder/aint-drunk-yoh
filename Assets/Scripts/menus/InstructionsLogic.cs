﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InstructionsLogic : MonoBehaviour
{

    private List<GameObject> groups = new List<GameObject>();
    private int current = 0;
    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject o = transform.GetChild(i).gameObject;
            groups.Add(o);
            o.SetActive(i == 0);
        }
    }

    void Update()
    {
        if (Input.GetButton("Cancel"))
        {
            SceneManager.LoadScene("main-menu");
            return;
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow)) OnClick();
        else if (Input.GetKeyDown(KeyCode.LeftArrow)) BackOneSlide();
    }

    void BackOneSlide()
    {
        if (current - 1 <= 0)
        {
            SceneManager.LoadScene("main-menu");
        }
        else
        {
            groups[current].SetActive(false);
            current--;
            groups[current].SetActive(true);
        }
    }

    public void OnClick()
    {
        if (current + 1 == groups.Count)
        {
            SceneManager.LoadScene("main-menu");
            return;
        }
        groups[current].SetActive(false);
        current++;
        groups[current].SetActive(true);
    }
}
