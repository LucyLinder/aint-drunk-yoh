﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[System.Serializable]
public class SceneInfo
{
    public string displayName;
    public string difficulty;
    public string id;
	public Sprite spritePreview;
}
public class MySliderMenu : SliderMenu
{

    [Header("Buttons")]
    public Button prevButton;
    public Button nextButton;

    [Header("Scenes")]
    public GameObject selectTrackViewPrefab;
    public SceneInfo[] scenes;

   
    private EventSystem eventSystem;
    
    protected override void Start()
    {
        eventSystem = FindObjectOfType<EventSystem>();

		LevelThumbnails = new List<GameObject>(scenes.Length);

		foreach(SceneInfo scene in scenes){
			GameObject o = Instantiate(selectTrackViewPrefab, ScrollContent.transform);
			SelectTrackView view = o.GetComponent<SelectTrackView>();
			if(view != null) view.Init(scene);
			LevelThumbnails.Add(o);
		}

        if(nextButton != null) nextButton.onClick.AddListener(NextButton);
        if(prevButton != null) prevButton.onClick.AddListener(PreviousButton);

        base.Start();
    }

    protected override void Update()
    {
        GameObject current = CurrentThumbnail;
        base.Update();
        if(current != CurrentThumbnail){
            Button target = CurrentThumbnail.GetComponentInChildren<Button>();
            eventSystem.SetSelectedGameObject(target.gameObject);
        }
    }
}
