﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectTrackView : MonoBehaviour
{

    public Text trackTitleText;
    public Text difficultyText;
    public Text bestTimeText;
    public Image previewImage;
    public Button showScoresButton;
    public string sceneToLoad;

    public void Init(SceneInfo sceneInfo)
    {
        trackTitleText.text = sceneInfo.displayName;
        difficultyText.text = sceneInfo.difficulty;
        if (sceneInfo.spritePreview != null) {
            previewImage.sprite = sceneInfo.spritePreview;
            previewImage.preserveAspect = true;
        }
        sceneToLoad = sceneInfo.id;
    }

    void Start()
    {
        bestTimeText.text = "Best Time: " + ScoresManager.GetBestScore(sceneToLoad);
        Button b = previewImage.gameObject.GetComponent<Button>();
        if (b != null)
        {
            b.onClick.AddListener(OnClick);
        }
        if(showScoresButton != null){
            showScoresButton.onClick.AddListener(showScorePanel);
        }
    }

    void OnClick()
    {
        Debug.Log("loading scene " + sceneToLoad);
        SceneManager.LoadScene(sceneToLoad);
    }

    void showScorePanel(){
        Debug.Log("show score panel => " + sceneToLoad);
        FindObjectOfType<ChooseTrackMenuLogic>().ShowScoresPanel(sceneToLoad);
    }

}
