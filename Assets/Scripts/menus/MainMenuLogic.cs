﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuLogic : MonoBehaviour
{
    public GameObject mainPanel;
    public GameObject creditsPanel;
    public GameObject controlsPanel;
    private EventSystem eventSystem;

    // Use this for initialization
    void Start()
    {
        eventSystem = FindObjectOfType<EventSystem>();
        OnBackButtonClick();
    }
	
	void Update() {
		if (!mainPanel.activeSelf && 
            (Input.GetButtonDown("Cancel") || Input.GetAxis("Horizontal") < 0)){
			OnBackButtonClick();
		}
	}

    public void OnPlayButtonClick()
    {
        SceneManager.LoadScene("choose-track-menu");
    }

    public void OnHowtoButtonClick()
    {
        SceneManager.LoadScene("instructions");
    }

    public void OnCreditsButtonClick()
    {
        enablePanel(creditsPanel);
    }

    public void OnControlsButtonClick()
    {
        enablePanel(controlsPanel);
    }

    public void OnBackButtonClick()
    {
        enablePanel(mainPanel);
    }

    public void OnExitButton()
    {
        Application.Quit();
    }

    private void enablePanel(GameObject panel)
    {
        disableAll();
        panel.SetActive(true);
        eventSystem.SetSelectedGameObject(panel.GetComponentInChildren<Button>().gameObject);
    }

    private void disableAll()
    {
        mainPanel.SetActive(false);
        creditsPanel.SetActive(false);
        controlsPanel.SetActive(false);
    }
}
