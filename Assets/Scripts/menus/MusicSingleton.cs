﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicSingleton : MonoBehaviour
{

    public static MusicSingleton instance;
    public AudioClip menuMusic;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Debug.Log("start music DESTROY");
            Destroy(this.gameObject);
            return;
        }
        else
        {
            Debug.Log("start music");
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

    }


    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start 
        // listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change 
        // as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("LOADED " + scene.name + " " + instance.name);
        if (!(scene.name.Contains("menu") ||
              scene.name.Contains("instruction")))
        {
            Destroy(this.gameObject);
        }
    }

}
