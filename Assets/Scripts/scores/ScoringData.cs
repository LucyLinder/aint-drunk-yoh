﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Linq;
using System.Text;

class ScoringData
{

	private static readonly int MAX_SCORES_KEPT = 10;
	public float[] Scores;
	public string[] Stats;

	private const string PREF_KEY_SCORE = "score";
	private const string PREF_KEY_STATS = "stats";

	private static ScoringData scoringData;

	public static ScoringData Instance {
		get {
			if (scoringData == null)
				scoringData = new ScoringData ();
			return scoringData;
		}

	}

	private  ScoringData ()
	{
		Scores = PlayerPrefsX.GetFloatArray (PREF_KEY_SCORE);
		Stats = PlayerPrefsX.GetStringArray (PREF_KEY_STATS);
	}
		
	public void AddScore (float time, params int[] metas)
	{
		Stats = new List<string> (Stats) { Utils.TimeNow () + "#" + string.Join ("#", metas.Select (x => x.ToString ()).ToArray ()) }.ToArray ();
		Scores = new List<float> (Scores) { time }.ToArray ();

		Array.Sort<float, string> (Scores, Stats);
		Array.Reverse (Scores);
		Array.Reverse (Stats);

		Stats = Stats.Take (MAX_SCORES_KEPT).ToArray ();
		Scores = Scores.Take (MAX_SCORES_KEPT).ToArray ();

		PlayerPrefsX.SetFloatArray (PREF_KEY_SCORE, Scores);
		PlayerPrefsX.SetStringArray (PREF_KEY_STATS, Stats);
	}

	public string GetFormatedScoreList ()
	{
		if (Stats.Length == 0) {
			return "No scores, yet.";
		}

		string ret = "";
		for (int i = 0; i < Scores.Length; i++) {
			string[] metas = Stats [i].Split ('#'); 
			ret += String.Format ("<b>{0}</b>  minutes -  purged: <b>{2,4}</b>/{3,-3} ({1})\n", Utils.FormatTime (Scores [i]), metas [0], metas [1], metas [2]);
		}
		return ret;
	}


}

