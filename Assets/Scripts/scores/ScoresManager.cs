﻿using System.Collections.Generic;
using UnityEngine;

using System;
using System.Linq;

class ScoresManager
{

    private static Dictionary<string, ScoreData> scores = new Dictionary<string, ScoreData>();

    public static bool AddScore(string level, float time, params int[] metas)
    {
        ScoreData score = getScoreData(level);
        return score.AddScore(time, metas);
    }

	public static string GetBestScore(string level)
    {
        ScoreData score = getScoreData(level);
        return score.GetBestScore();
    }

    public static ScoreData getScoreData(string level)
    {
		Debug.Log("get score data => " + level);
        if (scores.ContainsKey(level)) return scores[level];

        ScoreData score = new ScoreData(level);
        scores[level] = score;
        return score;
    }

    public static void Rename(string oldLevelName, string newLevelName){
        ScoreData score = getScoreData(oldLevelName);
        score.RenameLevel(newLevelName);
        Debug.Log("renamed scores " + oldLevelName + " to " + newLevelName);
    }

    public static void DeleteAll(){
        PlayerPrefs.DeleteAll();
        scores = new Dictionary<string, ScoreData>();
    }
}

class ScoreData
{
    private static readonly int MAX_SCORES_KEPT = 10;

    public float[] Scores;
    public string[] Stats;

    public float bestScore = -1;

    private const string PREF_KEY_SCORE_PREFIX = "scores_";
    private const string PREF_KEY_STATS_PREFIX = "stats_";

    public string levelName;

    string ScoreKey { get { return PREF_KEY_SCORE_PREFIX + levelName; } }
    string StatsKey { get { return PREF_KEY_STATS_PREFIX + levelName; } }

    public ScoreData(string level)
    {
        levelName = level;
		Debug.Log("loading scores for " + ScoreKey);
        Scores = PlayerPrefsX.GetFloatArray(ScoreKey);
        Stats = PlayerPrefsX.GetStringArray(StatsKey);
        if (Scores.Length > 0) bestScore = Scores[0];
    }

    public void RenameLevel(string newLevel){
        levelName = newLevel;
        persist();
    }

    public bool AddScore(float time, params int[] metas)
    {

        Stats = new List<string>(Stats) { Utils.TimeNow() + "#" + string.Join("#", metas.Select(x => x.ToString()).ToArray()) }.ToArray();
        Scores = new List<float>(Scores) { time }.ToArray();

        Array.Sort<float, string>(Scores, Stats);

        Stats = Stats.Take(MAX_SCORES_KEPT).ToArray();
        Scores = Scores.Take(MAX_SCORES_KEPT).ToArray();

        persist();

        if (bestScore < 0 || time < bestScore)
        {
            bestScore = time;
            return true;
        }
        return false;
    }

    private void persist(){
        PlayerPrefsX.SetFloatArray(ScoreKey, Scores);
        PlayerPrefsX.SetStringArray(StatsKey, Stats);
    }

    public string GetBestScore()
    {
        return bestScore < 0 ? "-" : Utils.FormatTime(Scores[0]);
    }

}

