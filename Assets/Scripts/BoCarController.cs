﻿using System;
using UnityEngine;

public class BoCarController : MonoBehaviour {

   [Header("Aint-Drunk-Yoh")]
    public float consumption = 5;

	[NonSerialized] public bool inverseH;
	[NonSerialized] public bool inverseV;

    protected Vector3 lastPosition; //this will just store our last position
    protected Vector3 currentPosition; //transform.position
	
	protected GameLogic logic;
	protected Tank tank;

	protected virtual void Start(){
		tank = FindObjectOfType<Tank>();
        logic = FindObjectOfType<GameLogic>();
		lastPosition = currentPosition = transform.position;
	}

    protected void consumeGas()
    {
        currentPosition = transform.position;
        currentPosition.y = 0;
        float change = Vector3.Distance(currentPosition, lastPosition);
        tank.Consume(change * consumption);
        lastPosition = currentPosition;

    }
	
    public virtual void ResetPosition(Vector3 position, Quaternion rotation)
    {
        lastPosition = position;
        transform.position = position;
        transform.rotation = rotation;
    }

}
